local _G = GLOBAL
local TheSim = _G.TheSim
local TheNet = _G.TheNet
local TheInput = _G.TheInput

if TheNet and ((TheNet:GetIsServer() and TheNet:GetServerIsDedicated()) or
               (TheNet:GetIsClient() and not TheNet:GetIsServerAdmin())) then
    return
end

local COPY_KEY = GetModConfigData("COPY_KEY") or "KEY_O"
local WRAP_TO_GIFT = GetModConfigData("WRAP_TO_GIFT") or false
local SAY_NOTHAVE_BUNDLE = "I not have bundle in first slot inventory"
local script_wrap = ''

if not WRAP_TO_GIFT then
    script_wrap = ' if bundle.prefab == "bundle" then prefabreplica = "bundle" end'
end

local function SendCommand(command)
    local x, _, z = TheSim:ProjectScreenPos(TheSim:GetPosition())
    if TheNet:GetIsClient() and TheNet:GetIsServerAdmin() then
        TheNet:SendRemoteExecute(command, x, z)
    else
        _G.ExecuteConsoleCommand(command)
    end
end

TheInput:AddKeyUpHandler(_G[COPY_KEY], function(key)
    local ThePlayer = _G.ThePlayer
    if ThePlayer ~= nil and not ThePlayer.HUD:IsChatInputScreenOpen() then
        SendCommand(string.format('local player = UserToPlayer("%s") local inventory = player and player.components.inventory or nil if inventory then local bundle = inventory:GetItemInSlot(1) if bundle and bundle.components.unwrappable then local pos = player:GetPosition() local items = {} for _, v in ipairs(bundle.components.unwrappable.itemdata) do local item = SpawnPrefab(v.prefab, v.skinname, v.skin_id, nil) if item ~= nil and item:IsValid() then item:SetPersistData(v.data) table.insert(items, item) end end local prefabreplica = "gift"'..script_wrap..' local bundlereplica = SpawnPrefab(prefabreplica) bundlereplica.components.unwrappable:WrapItems(items) for _, v in ipairs(items) do v:Remove() end if bundlereplica.Physics ~= nil then bundlereplica.Physics:Teleport(pos:Get()) else bundlereplica.Transform:SetPosition(pos:Get()) end if bundlereplica.components.inventoryitem ~= nil then bundlereplica.components.inventoryitem:OnDropped(true, 1) end elseif player.components.talker then player.components.talker:Say("%s") end end', ThePlayer.userid, SAY_NOTHAVE_BUNDLE))
    end
end)
